#include <iostream>
#include <string>
#include "leveldb/db.h"

void addName(leveldb::DB *);
void delName(leveldb::DB *, const std::string);
void showName(leveldb::DB *, const std::string);
void end();
