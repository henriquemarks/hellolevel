#include <iostream>
#include <string>
#include "leveldb/db.h"
#include "db.h"

using namespace std;

int main(void)
{
    leveldb::DB *db;
    leveldb::Options options;
    options.create_if_missing = true;

    // open
    leveldb::Status status = leveldb::DB::Open(options, "/tmp/testdb", &db);
    assert(status.ok());

    const string key = "name";

    int choice;

    for (;;) {
        cout << "Options" << endl;
        cout << "1 - Add Name" << endl;
        cout << "2 - Delete Name" << endl;
        cout << "3 - Show Name" << endl;
        cout << "4 - Clear Database" << endl;
        cout << "5 - Exit" << endl;
        cout << "Choose Option" <<endl;

        cin >> choice;

        switch (choice) {
            case 1:
                addName(db);
                break;
            case 2:
                delName(db, key);
                break;
            case 3:
                showName(db, key);
                break;
            case 4:
                leveldb::DestroyDB("/tmp/testdb", options);
                break;
            case 5:
                delete db;
                exit(0);
                break;
        }
    }

    return 0;
}

// Compile:
// mkdir build
// cd build
// cmake ..
// cmake --build .
// Run: ./hello_level

