#include "db.h"
#include "leveldb/db.h"
#include <assert.h>

using namespace std;

void addName(leveldb::DB *db)
{
    string value;
    cout << "Name to add: ";
    cin >> value;

    const string key = "name";

    leveldb::Status status;
    status = db->Put(leveldb::WriteOptions(), key, value);
    assert(status.ok());
}

void delName(leveldb::DB *db, const string key)
{
    leveldb::Status status;
    status = db->Delete(leveldb::WriteOptions(), key);
    assert(status.ok());
}

void showName(leveldb::DB *db, const string key)
{
    leveldb::Status status;
    string value;
    status = db->Get(leveldb::ReadOptions(), key, &value);
    if (status.ok()) {
        cout << "key: " << key << " | " << "value: " << value << endl;
    }
    else
    {
        cout << "Empty Table" << endl;
    }
}
